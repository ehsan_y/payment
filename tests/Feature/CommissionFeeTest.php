<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CommissionFeeTest extends TestCase
{
    public function testCommissionIsTestedCorrectly()
    {
    $response = $this->json('post', "commission-fee",
        ["input" =>
            "2014-12-31,4,private,withdraw,1200.00,EUR"
            ."\n".
            "2015-01-01,4,private,withdraw,1000.00,EUR"
            ."\n".
            "2016-01-05,4,private,withdraw,1000.00,EUR"
            ."\n".
            "2016-01-05,1,private,deposit,200.00,EUR"
            ."\n".
            "2016-01-06,2,business,withdraw,300.00,EUR"
            ."\n".
            "2016-01-06,1,private,withdraw,30000,JPY"
            ."\n".
            "2016-01-06,1,private,withdraw,30000,JPY"
            ."\n".
            "2016-01-07,1,private,withdraw,100.00,USD"
            ."\n".
            "2016-01-10,1,private,withdraw,100.00,EUR"
            ."\n".
            "2016-01-10,2,business,deposit,10000.00,EUR"
            ."\n".
            "2016-01-10,3,private,withdraw,1000.00,EUR"
            ."\n".
            "2016-02-15,1,private,withdraw,300.00,EUR"
            ."\n".
            "2016-02-19,5,private,withdraw,3000000,JPY"
    ]);
    $response->assertStatus(200);
    }
}
