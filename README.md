**By EY, mailbox5517@gmail.com**

#### payment

this project is done using a laravel framework the last dev version
- after pulling the project
- composer install [or php composer.phar install]
- php artisan key:generate
- php artisan serv
- php artisan config:cache
- php artisan test 
- in case of any access denied error on linux please change the access of the related directory


#### test 


there is a test that takes the aggregated input and returns a sery of output which can be customized in the related test file. to run this test and see the result run the "php artisan test" command in a terminal at root directory of the project.  


#### config info

there are many useful configurations in the .env file that also have equivalent confin file named "all.config"

#### service oriented architecture
there is a ***service***  directory that contains all commission related classes like factory traits repository and other logic classes 


####  In-memory storage 

the cache helper of the frame work is used which is configured to  use only the in memory file systemhe  of the server OS.
