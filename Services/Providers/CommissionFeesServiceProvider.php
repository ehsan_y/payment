<?php

namespace Services\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Collection;

class CommissionFeesServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Automatically apply the service configuration
        $this->mergeConfigFrom(__DIR__.'/../CommissionFees/config/all.php', 'all');
    }
}
