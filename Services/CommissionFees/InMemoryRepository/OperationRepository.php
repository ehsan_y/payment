<?php

namespace Services\CommissionFees\InMemoryRepository;

use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

class OperationRepository extends BaseRepository
{
    /**
     * Adding a new operation to in-memory operations
     * @param $userId
     * @param $accountOperation
     * @return bool
     */
    public function create($operation): bool
    {
        $timeToLive = config(' all.timeLimit');
        if ( Cache::has($operation->userId)) {
            $this->addOperation($operation, $timeToLive);
        } else {
            Cache::put('UserId', $operation->userId);
            $this->addOperation($operation, $timeToLive);
        }
    return true;
    }

    /**
     * @param $operation
     * @param $timeToLive
     */
    private function addOperation($operation, $timeToLive)
    {
        $operations = (array) $this->findOperationsByUserId($operation->userId);
        array_push( $operations , $operation );
        Cache::put($operation->userId, $operations, $timeToLive);
    }
    /**
     * @param $userId
     * @return array|mixed
     */
    public function findOperationsByUserId($userId)
    {
        $userOperations = Cache::get($userId);
        return $userOperations;
    }

    /**
     * @param $operation
     * @return array
     */
    public function findWithdrawsByUserIdInSameWeek($operation)
    {
         $olderOperations = $this->findOperationsByUserId($operation->userId);
        if($olderOperations == null) {
            return null;
        }
        $operationDate = Carbon::createFromFormat('Y-m-d', $operation->date);
        $weekStartDate = $operationDate->startOfWeek()->format('Y-m-d');
        $weekEndDate = $operationDate->endOfWeek()->format('Y-m-d');
        $operationName = $operation->operation;

        $result = array();
        foreach($olderOperations as $olderOperation)
            {
                if($olderOperation->operation == self::WITHDRAW)
                    if($operationDate >= $weekStartDate)
                        $result[] = $olderOperation;
            }
        return $result;
    }
}
