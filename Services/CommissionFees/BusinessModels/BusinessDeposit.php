<?php

namespace Services\CommissionFees\BusinessModels;

use Illuminate\Support\Facades\Cache;

    /*
     * this class will be created by factory.
     * since it has the same logic as Deposit
     * it inherits all methods from Deposit
     */
class BusinessDeposit extends Deposit
{
    public function __construct($operation = null)
    {
        $this->operation = $operation;
    }
    public function index($operation)
    {
        $this->getOperation($operation);
        $businessDeposit = config('all.businessDeposit');
        return ($this->operation->amount*$businessDeposit)/100;
    }
    public function getOperation($operation)
    {
        $this->operation = $operation;
    }
}
