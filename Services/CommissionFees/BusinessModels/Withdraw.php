<?php

namespace Services\CommissionFees\BusinessModels;

use Illuminate\Support\Facades\Cache;

class Withdraw
{
    //This is the base currency.
    public const BASE_CURRENCY = 'EUR';

    public function __construct($operation=null)
    {
        $this->operation = $operation;
    }
    public function index($operation=null)
    {
        //
    }
    public function getOperation($operation)
    {
        $this->operation = $operation;
    }
}
