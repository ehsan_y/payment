<?php

namespace Services\CommissionFees\BusinessModels;

use Illuminate\Support\Facades\Cache;

    /*
     * this class will be created by factory.
     * since it has the same logic as Deposit
     * it inherits all methods from Deposit
     */
class PrivateDeposit extends Deposit
{
    public function __construct($operation = null)
    {
        $this->operation = $operation;
    }
    public function index($operation)
    {
        $this->getOperation($operation);
        $privateDeposit = config('all.privateDeposit');
        return ($this->operation->amount*$privateDeposit)/100;
    }
    public function getOperation($operation)
    {
        $this->operation = $operation;
    }
}
