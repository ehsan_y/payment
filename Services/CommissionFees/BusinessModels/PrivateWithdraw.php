<?php

namespace Services\CommissionFees\BusinessModels;

use Services\CommissionFees\Tools\CurrencyConvertorTrait;
use Services\CommissionFees\Verifications\WithdrawAmount;
use Services\CommissionFees\Verifications\WithdrawRepetition;
use Services\CommissionFees\InMemoryRepository\OperationRepository;////
class PrivateWithdraw extends Withdraw
{
    use CurrencyConvertorTrait;


    public $withdrawRepetition;

    public function index($operation=null)
    {
         $repetitions = $this->verifyRepetitions($operation);
         $repetitionsChecked = $this->isACommissionFreeRepetition($repetitions);
        /*
            * getting the total withdrawn amount per week
            * (The value is returned, in the base currency: EUR!)
            * and maximum free of charge allowd amount.
            */
        $withdrawAmount = new WithdrawAmount;
        $withdrawnAmount = $withdrawAmount->index($operation);

        $currentAmountForWithdraw = $this->getAmount($operation->amount)
            ->getOrigin($operation->currency)
            ->getDestination(self::BASE_CURRENCY)
            ->convert();

        if($repetitionsChecked)
         {
             $limit = config('all.withdrawAmount');
             $remainedChargeFree   = $limit - $withdrawnAmount;
            /*
             * $amount variable holds the commission
             * chargable amount.
             */
             $chargeableAmountBase = $currentAmountForWithdraw - $remainedChargeFree;
             if($withdrawnAmount < $limit)
             {
                 if ($chargeableAmountBase <= 0)
                 {
                     $chargeableAmountBase = 0;
                 }
             } else {
                 $chargeableAmountBase = $currentAmountForWithdraw;;
             }
        } else {
            $chargeableAmountBase = $currentAmountForWithdraw;
        }
        /*
         * returning "currency" of $amount from 'base' to it's 'original'
         */

        $chargeableAmountOriginal = $this->getAmount($chargeableAmountBase)
            ->getOrigin(self::BASE_CURRENCY)
            ->getDestination($operation->currency)
            ->convert();
        /*
         * calculating the fee
         */
        return $CommissionFee =
            $chargeableAmountOriginal*
            config('all.privateWithdraw')/100;
    }

    /**
     * @param $repetitions
     * @return bool
     */
    public function isACommissionFreeRepetition($repetitions)
    {
        return $repetitions < config('all.withdrawRepetition');
    }

    /**
     * @param $operation
     * @return int
     */
    public function verifyRepetitions($operation)
    {
        $this->withdrawRepetition = new WithdrawRepetition;
        return $count = $this->withdrawRepetition->index($operation);
    }
}
