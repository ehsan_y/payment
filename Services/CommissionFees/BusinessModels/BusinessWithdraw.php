<?php

namespace Services\CommissionFees\BusinessModels;

use Illuminate\Support\Facades\Cache;

class BusinessWithdraw extends Withdraw
{
    public function index($operation=null)
    {
        $this->getOperation($operation);
        $privateDeposit = config('all.businessWithdraw');
        return $this->operation->amount*$privateDeposit;
    }
}
