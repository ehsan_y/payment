<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Commission Fee Configs
    |--------------------------------------------------------------------------
    | The maximum time of each operation to live in
    | memory may be defined in .env file as
    | COMMISSION_FEE_OPERATIONS_TTL.  it will
    | be cached just like other framework's configs for performance
    | considerations as a config value  named 'timeLimit'.
    | (in this project it is a maximum of 7 days)
    |
    */

    'timeLimit' => env('COMMISSION_FEE_OPERATIONS_TTL'),
    /*
    |--------------------------------------------------------------------------
    | Commission Fee Configs
    |--------------------------------------------------------------------------
    | The dynamic commission fee percentages of
    |  different operations and limitations should be valued
    | in the .env file of the framework

    */

    'privateDeposit' => env('COMMISSION_PERCENT_FOR_PRIVATE_DEPOSIT'),
    'businessDeposit' => env('COMMISSION_PERCENT_FOR_BUSINESS_DEPOSIT'),
    'privateWithdraw' => env('COMMISSION_PERCENT_FOR_PRIVATE_WITHDRAW'),
    'businessWithdraw' => env('COMMISSION_PERCENT_FOR_BUSINESS_WITHDRAW'),

    'withdrawAmount' => env('COMMISSION_FREE_AMOUNT_LIMIT_FOR_PRIVATE_WITHDRAW'),
    'withdrawRepetition' => env('COMMISSION_FREE_REPETITION_FOR_PRIVATE_WITHDRAW'),
        /*
        |--------------------------------------------------------------------------
        | Commission Fee Configs
        |--------------------------------------------------------------------------
        | URL of Currency rates.
        | and
        | ttl for the Currency rates in memory.
        |
        */
    'currencyUrl' => env('COMMISSION_FEE_CURRENCY_URL'),
    'currencyTimeLimit' => env('COMMISSION_FEE_CURRENCY_TTL'),



];
