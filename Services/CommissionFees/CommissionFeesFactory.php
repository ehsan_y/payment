<?php

//declare(strict_types=1);

namespace Services\CommissionFees;

class CommissionFeesFactory  //extends AbstractFactory...
{
    public function proccessOperation($operation)
    {
        $class = $this->makeClassName($operation->type, $operation->operation);
        $instance = new $class;
        return $instance->index($operation);
    }
    public function makeClassName($type, $operation)
    {
        return 'Services\CommissionFees\BusinessModels'.'\\'
            .$this->upperCase($type).
            $this->upperCase($operation);
    }
    public function upperCase($str)
    {
        $str = strtolower($str);
        return ucfirst($str);
    }
}

