<?php

namespace Services\CommissionFees\Tools;

use Illuminate\Support\Facades\Cache;

class DataObject
{
    private $operation;

    private const DATE = 'date';

    private const USER_ID = 'userId';

    private const TYPE = 'type';

    private const OPERATION = 'operation';

    private const AMOUNT = 'amount';

    private const CURRENCY = 'currency';

    public function prepareData($CSV): object
    {
        //keys
        $keys = $this->getKeys();

        //values
        $inputs = $this->explodePerLines($CSV);

        $preparedData = $this->explodePerFieldsAndCombine($keys, $inputs);
        return $this->operation = collect($preparedData);
    }
    public function getKeys(): array
    {
        return [
            self::DATE,
            self::USER_ID,
            self::TYPE,
            self::OPERATION,
            self::AMOUNT,
            self::CURRENCY
        ];
    }
    public function explodePerLines($CSV): array
    {
        return explode("\n",  $CSV);
    }
    public function explodePerFieldsAndCombine($keys, $inputs): array
    {
        foreach ($inputs as $input)
        {
            $values = explode(",", $input);
            $data[] = (object)array_combine($keys, $values);
        }
        return $data;
    }
}

