<?php

namespace Services\CommissionFees\Tools;

use Illuminate\Support\Facades\Cache;

trait CurrencyConvertorTrait
{
    private $origin;

    private $destination;

    private $amount;

    /**
     * converting "currency" of amount from 'origin' to it's 'destination'
     *
     * @return float|int
     */
    public function convert()
    {
        /*
         * here it will return the amount value,
         * if origin and destination are the same
         */
        if ($this->origin === $this->destination) {
            return $amount = $this->amount;
        /*
         * the main conversion
         */
        } else {
            $convertions = $this->bringConversions();
            $convertions = json_decode($convertions);
            return $amount =
                $this->amount * $convertions->rates->{$this->origin};
        }


    }
    public function bringConversions()
    {
        if ( Cache::has('currencies')) {
            return $currencies = Cache::get('currencies');
        }
            $url = config('all.currencyUrl');
            $currencies = file_get_contents($url);
            Cache::put('currencies', $currencies, config('all.currencyTimeLimit'));
            return $currencies;
    }
    public function getOrigin($origin)
    {
         $this->origin = $origin;
        return $this;
    }
    public function getDestination($destination)
    {
         $this->destination = $destination;
        return $this;
    }
    public function getAmount($amount)
    {
         $this->amount = $amount;
         return $this;
    }


}
