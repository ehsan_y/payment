<?php

namespace Services\CommissionFees\Verifications;

use Services\CommissionFees\InMemoryRepository\OperationRepository as Repository;
use Services\CommissionFees\Tools\CurrencyConvertorTrait;

class WithdrawAmount
{
    use CurrencyConvertorTrait;

    public const WITHDRAW = 'withdraw';

    public const BASE_CURRENCY = 'EUR';

    public $withdrawnInBaseCurrency = 0;

    /**
     * @param $operation|null
     * @return integer
     */
    public function index($operation)
    {
        $repository = new Repository;
        $relatedOperations = $repository->findWithdrawsByUserIdInSameWeek($operation);
        if(empty($relatedOperations)) {
            return $withdrawnInBaseCurrency = 0;
        }
        foreach($relatedOperations as $relatedOperation)
        {
            $atomicAmount = $this->getAmount($relatedOperation->amount)
                ->getOrigin($relatedOperation->currency)
                ->getDestination(self::BASE_CURRENCY)
                ->convert();
            $this->withdrawnInBaseCurrency = $this->withdrawnInBaseCurrency + $atomicAmount;
        }
        return $this->withdrawnInBaseCurrency;
    }
}
