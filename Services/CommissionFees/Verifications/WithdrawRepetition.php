<?php

namespace Services\CommissionFees\Verifications;

use Illuminate\Support\Carbon;
use Services\CommissionFees\InMemoryRepository\OperationRepository as Repository;

class WithdrawRepetition
{
    public const WITHDRAW = 'withdraw';


    /**
     * @param $operation|null
     * @return integer
     */
    public function index($operation=null): int
    {
        $repository = new Repository;
        $relatedOperations =(array) $repository->findWithdrawsByUserIdInSameWeek($operation);
        $count = count($relatedOperations);
        return $count;
    }
}
