<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Services\CommissionFees\InMemoryRepository\OperationRepository;
use Services\CommissionFees\Tools\DataObject;
use Services\CommissionFees\CommissionFeesFactory as Factory;

class CommissionFeeController extends Controller
{
    public $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }
    public function index(DataObject $dataObject, OperationRepository $repository, Factory $factory)
    {
        $CSV = $this->request->input('input');
         $DTO = $dataObject->prepareData($CSV);
        $commisionfees = [];
        foreach($DTO as $operation)
        {
            $commisionfees[] = $factory->proccessOperation($operation);

            /* adding the new operation to memory */
            $repository->create($operation);
        }
        foreach($commisionfees as $commisionfee) {
            $commisionfee = round($commisionfee, 2);
            echo $commisionfee
            ,"\n";
        }
    }
}
